from venv import create
from django.contrib import admin
from django.urls import path, include
from books1.views import show_books
from books1.views import create_book
from books1.views import show_one_book
from books1.views import update_book
from books1.views import delete_book
from books1.views import show_mags, create_mag, show_one_mag, update_mag, delete_mag, show_one_genre
#from books1.views import create_view

urlpatterns = [
    path("", show_books, name = "showbooks"),
 #   path("geeks", create_view),
    path("create/", create_book, name = "createbook"),
    path("<int:pk>/", show_one_book, name="book_detail"),
    path("<int:pk>/edit/", update_book, name ="edit_book"),
    path("<int:pk>/delete/", delete_book, name = "delete_book"),
    path("mags/", show_mags, name = "showmags"),
    path("createmag/", create_mag, name = "createmag"),
    path("mag<int:pk>/", show_one_mag, name="mag_detail"),
    path("<int:pk>/editmag/", update_mag, name ="edit_mag"),
    path("<int:pk>/deletemag/", delete_mag, name = "delete_mag"),
    path("genre<int:pk>/", show_one_genre, name="genre_detail"),
    


#"*" anything; if there is nothing else ""
] # when you include the / it'll tak into considersation both 
# entries of create/ and create 


#you always need to import the view function you are referencing
#add the path you want: path("name on browser", view function name, name)