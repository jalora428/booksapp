from django.contrib import admin
from books1.models import Book #import the  model
from books1.models import BookReview
from books1.models import Mag
from books1.models import Author
from books1.models import Issues
from books1.models import Genres


#you would create a class
class BookAdmin(admin.ModelAdmin): #can edit how it's displayed; adds features
        pass #(optional?) this represents the admin interface for the model;

admin.site.register(Book, BookAdmin) #register it onto  the actual page; 
# Register your model here.

admin.site.register(BookReview)
admin.site.register(Mag)
admin.site.register(Author)
admin.site.register(Issues)
admin.site.register(Genres)

