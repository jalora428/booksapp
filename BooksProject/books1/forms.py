from django import forms
#from .models import GeeksModel
from .models import Book, Mag
  
  
# creating a form
# class GeeksForm(forms.ModelForm):
  
    # create meta class
#    class Meta:
        # specify model to be used
#        model = GeeksModel
  
        # specify fields to be used
 #       fields = [
  #          "title",
   #         "description",
    #    ]

class BookForm(forms.ModelForm):

    class Meta:
        model = Book
        exclude = []
        
  #      fields = [
  #          "title",
  #          "author",
  #          "number_pages",
  #          "isbn",
  #          "description",
  #          "year_published",
  #          "image"
  #      
  #      ]

class MagForm(forms.ModelForm):
    class Meta:
        model = Mag
        exclude = []