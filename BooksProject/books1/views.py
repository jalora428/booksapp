from nturl2path import url2pathname
from django.shortcuts import redirect, render, get_object_or_404
from books1.models import Book #imported Book class #place all the models here
from books1.forms import BookForm, MagForm
from books1.models import Mag
from books1.models import Genres


# relative import of forms
#from books1.models import GeeksModel
#from books1.forms import GeeksForm

# Create your views here.
#components needed for view function to display all of the books:
    #import the book
    #queryset method: book.objects.all()
    #return render(request, template, context)

def show_books(request):
    books = Book.objects.all() #gets the books
    context = {
        "books": books #this is a dictionary with the value books & key "books"; 
        #the value books is the information from the class;
    }   #the render returns the response to the client/web browser
    return render(request, "books/list.html", context) 
    # "" has the path of the html file you want to function to work in
    # only include the directory directly containing the html file and the html file

#def create_view(request):
    # dictionary for initial data with 
    # field names as keys
 #   context ={}
  
    # add the dictionary during initialization
  #  form = GeeksForm(request.POST or None)
   # if form.is_valid():
   #     form.save()
          
    #context['form']= form
    #return render(request, "books/create_view.html", context)

#def create_book(request):
 #   context ={}
 #   form = BookForm(request.POST or None)
 #   if form.is_valid():
 #       if request.method == "POST":
 #           return redirect('/books/')
 #       else:
 #           form.save()
 #       

 #   context['form'] = form
 #   return render(request, "books/create.html", context)

#if it posts redirect to url
#else return the same page

def create_book(request):
    context ={}
    form = BookForm(request.POST or None)
    if form.is_valid():
        book_detail_instance = form.save()
        return redirect("book_detail", book_detail_instance.pk)
        
    context['form1'] = form
    return render(request, "books/create.html", context)


def show_one_book(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book": book
    }
    return render(request, "books/detail.html", context)


def update_book(request, pk):
    if Book and BookForm:
        instance = Book.objects.get(pk=pk)
        if request.method == "POST":
            form = BookForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
            return redirect("showbooks")
        else:
            form = BookForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "books/edit.html", context)



def delete_book(request, pk):
    context ={}
    obj = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("showbooks")
    return render(request, "books/delete.html", context)







def show_mags(request):
    mag = Mag.objects.all() #gets the books
    context = {
        "mags": mag #this is a dictionary with the value books & key "books"; 
        #the value books is the information from the class;
    }   #the render returns the response to the client/web browser
    return render(request, "mag/list.html", context) 


def create_mag(request):
    context ={}
    form = MagForm(request.POST or None)
    if form.is_valid():
        mag_detail_instance = form.save()
        return redirect("mag_detail", mag_detail_instance.pk)
        
    context['form1'] = form
    return render(request, "mag/create.html", context)


def show_one_mag(request, pk):
    mag = Mag.objects.get(pk=pk)
    context = {
        "mag": mag
    }
    return render(request, "mag/detail.html", context)


def update_mag(request, pk):
    if Mag and MagForm:
        instance = Mag.objects.get(pk=pk)
        if request.method == "POST":
            form = MagForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
            return redirect("showmags")
        else:
            form = MagForm(instance=instance)
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "mag/edit.html", context)


def delete_mag(request, pk):
    context ={}
    obj = get_object_or_404(Mag, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("showmags")
    return render(request, "mag/delete.html", context)


def show_one_genre(request, pk):
    genre = Genres.objects.get(pk=pk)
    context = {
        "genre": genre
    }
    return render(request, "mag/genre_detail.html", context)