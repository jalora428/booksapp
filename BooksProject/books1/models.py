from distutils.text_file import TextFile
from django.db import models
from django.forms import CharField, DateField


class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

# Create your models here.
class Book(models.Model): #book is the one in the one-to-many relationship
    title = models.CharField(max_length=200, unique=True)
    authors = models.ManyToManyField("Author", related_name="books")
    number_pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    
    def __str__(self): 
        return self.title + " by " + str(self.authors.first()) #this shows up on the admin page
# if you want to represent an instance of this model as a string, this is what
# it looks like; you return a string to see this class represented 
# srt(book_instance) will reference lines 14 & 15

#null and blank;

# declare a new model with a name "GeeksModel"
#class GeeksModel(models.Model):
  
    # fields of the model
 #   title = models.CharField(max_length = 200)
  #  description = models.TextField()
  
    # renames the instances of the model
    # with their title name
   # def __str__(self):
    #    return self.title


class Mag(models.Model):
    title = models.CharField(max_length=200, unique=True)
    release_cycle = models.TextField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    

    def __str__(self): 
        return self.title 



class BookReview(models.Model): #this is the Many in the one-to-many relationship
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField(null=True)


class Issues(models.Model):
    title = models.CharField(max_length=200, unique=True)
    description = models.TextField(null=True)
    page_count = models.SmallIntegerField(null=True)
    date = models.SmallIntegerField(null=True)
    mags = models.ForeignKey(Mag, related_name="issues", on_delete=models.CASCADE)


class Genres(models.Model):
    name = models.CharField(max_length=200, unique=True)
    mags = models.ManyToManyField("Mag", related_name="genre")



